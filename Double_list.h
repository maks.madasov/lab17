
#pragma once
#include <iostream>

template<typename T>
class Double_list 
{
private:

	class Node
	{
	public:
		Node* next;
		Node* prev;
		T value;

		Node() {
			next = NULL;
			prev = NULL;
			value = T();
		}

		Node(const Node* n) {
			next = n->next;
			prev = n->prev;
			value = n->value;
		}
	};

	Node* head;
	Node* tail;
	int size;

public:
	Double_list() {
		head = tail = NULL;
		size = 0;
	}
	
	Double_list(T val) {
		head = tail = new Node;
		tail->next = NULL;
		tail->prev = NULL;
		tail->value = val;
		size = 1;
	}

	Double_list(const Double_list& l) {
		if (l.head == NULL) {
			head = tail = NULL;
			size = 0;
		}
		else {
			size = l.size;

			Node* cur_l = l.head;

			Node* cur = new Node(cur_l);

			head = tail = cur;

			head->next = NULL;
			head->prev = NULL;

			while (cur_l->next != NULL)
			{
				cur->next = new Node(cur_l->next);
				cur->next->next = NULL;
				cur->next->prev = cur;

				tail = cur->next;
				cur_l = cur_l->next;
				cur = cur->next;
			}
		}
	}

	~Double_list() {
		Node* node_del = head;
		Node* cur = head;

		while (cur != tail)
		{
			cur = cur->next;
			delete node_del;
			node_del = cur;
		}

		delete node_del;
	}

	Double_list& operator=(const Double_list& l) {
		if (this == &l)
			return *this;

		this->~Double_list();

		if (l.head == NULL) {
			head = tail = NULL;
			size = 0;
		}
		else {
			size = l.size;

			Node* cur_l = l.head;

			Node* cur = new Node(cur_l);

			head = tail = cur;

			head->next = NULL;
			head->prev = NULL;

			while (cur_l->next != NULL)
			{
				cur->next = new Node(cur_l->next);
				cur->next->next = NULL;
				cur->next->prev = cur;

				tail = cur->next;
				cur_l = cur_l->next;
				cur = cur->next;
			}
		}
		
		return *this;
	}

	void push_font(T val) {
		Node* tmp = new Node;

		if (size == 0) {
			head = tail = tmp;
			tmp->next = NULL;
			tmp->prev = NULL;
			tmp->value = val;
			++size;
			return;
		}

		tmp->value = val;
		tmp->next = head;
		tmp->prev = NULL;

		head->prev = tmp;
		head = tmp;

		++size;
	}

	void push_back(T val) {
		Node* tmp = new Node;

		if (size == 0) {
			head = tail = tmp;
			tmp->next = NULL;
			tmp->prev = NULL;
			tmp->value = val;
			++size;
			return;
		}
		
		tmp->value = val;
		tmp->next = NULL;
		tmp->prev = tail;

		tail->next = tmp;
		tail = tmp;

		++size;
	}

	int get_size() { return size; }

	void find_and_erase(const T& val) {
		Node* cur = head;
		while (cur != NULL)
		{
			if (cur->value == val && cur == head && cur == tail) { // В списке только один элемент
				head = tail = NULL;
				delete cur;
				--size;
				return;
			}
			if (cur->value == val && cur == head) {	// Удаление первого элемента
				cur->next->prev = NULL;
				head = cur->next;
				delete cur;
				--size;
				return;
			}
			if (cur->value == val && cur == tail) { // Удаление последнего элемента
				cur->prev->next = NULL;
				tail = cur->prev;
				delete cur;
				--size;
				return;
			}
			if (cur->value == val) { // Удаление элемента в середине списка
				cur->prev->next = cur->next;
				cur->next->prev = cur->prev;
				delete cur;
				--size;
				return;
			}

			cur = cur->next;
		}
	}

	T at(int index) {
		if (index < 0 || index >= size)	throw "out_of_range";

		Node* cur = head;
		for (int i = 0; i < index; i++) {
			cur = cur->next;
		}
		return cur->value;
	}

	void print() {
		Node* cur = head;
		
		while (cur != NULL)
		{
			std::cout << cur->value << " ";
			cur = cur->next;
		}
		std::cout << "\n";
	}

};

struct Point {
	int x;
	int y;
	int z;

	Point(int X = 0, int Y = 0, int Z = 0) {
		x = X;
		y = Y;
		z = Z;
	}

	friend std::ostream& operator<<(std::ostream& out, const Point& p) {
		return (out << "(" << p.x << ", " << p.y << ", " << p.z << ")");
	}
};
