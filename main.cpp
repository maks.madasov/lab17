
#include "double_list.h"
#include <iostream>
#include <list>

int main() 
{ 
	Double_list<int> l1;
	l1.push_back(5);
	l1.push_font(2);
	l1.push_back(3);

	Double_list<int> l2 = l1;
	l2.push_font(10);
	l1.push_back(50);


	l1.print();
	l2.print();

	/*Double_list<int> l;
	l.push_font(11);
	l.find_and_erase(11);
	l.push_back(12);

	l.print();
	l.push_font(100);
	l.push_back(200);
	l.push_back(500);
	l.print();

	Double_list<int> l3(l);
	std::cout << "IT IS --> ";
	//l3.push_font(2);
	l3.print();

	try {
		int number = l.at(1);
		std::cout << number << "\n";
	}
	catch (const char* exception) {
		std::cerr << "Error: " << exception << "\n";
	}
	

	Double_list<Point> ll;
	ll.push_back(Point(11, 20, 30));
	ll.push_font(Point(50, 25, 10));
	ll.print();
	
	Double_list<Point> l2(ll);
	l2.push_back(Point(1, 2, 3));
	l2.print();*/


	/*std::list<int> list;
	list.push_back(10);
	list.push_back(20);

	std::list<int> list_2;
	list_2.push_back(100);

	list_2 = list;
	list_2.push_back(30);
	


	for (int n : list) {
		std::cout << n << " ";
	}
	std::cout << "\n";

	for (int n_2 : list_2) {
		std::cout << n_2 << " ";
	}
	std::cout << "\n";
	*/
	return 0;
}